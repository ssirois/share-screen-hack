rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
TEST_FILES:=$(call rwildcard,./tests/,test-*.sh)
TEST_TARGETS=$(patsubst %,test_%,$(TEST_FILES))

.PHONY: test
test: $(TEST_TARGETS)

test_%:
	$(*)

test_./%:
	$(*)

#!/bin/bash

A_CONNECTED_OUTPUT_NAME="a-connected-name"
A_CONNECTED_OUTPUT_WIDTH="1920"
A_CONNECTED_OUTPUT_HEIGHT="1080"
A_CONNECTED_OUTPUT_TOP="1000"
A_CONNECTED_OUTPUT_LEFT="1024"
A_CONNECTED_OUTPUT_GEOMETRY="${A_CONNECTED_OUTPUT_WIDTH}x${A_CONNECTED_OUTPUT_HEIGHT}+${A_CONNECTED_OUTPUT_TOP}+${A_CONNECTED_OUTPUT_LEFT}"
A_CONNECTED_OUTPUT="${A_CONNECTED_OUTPUT_NAME} connected ${A_CONNECTED_OUTPUT_GEOMETRY}"

ANOTHER_CONNECTED_OUTPUT_NAME="another-connected-name"
ANOTHER_CONNECTED_OUTPUT_GEOMETRY="640x480+0+0"
ANOTHER_CONNECTED_OUTPUT="${ANOTHER_CONNECTED_OUTPUT_NAME} connected ${ANOTHER_CONNECTED_OUTPUT_GEOMETRY}"

A_DISCONNECTED_OUTPUT="is disconnected"
ANOTHER_DISCONNECTED_OUTPUT="is also disconnected"

test_getConnectedOutputs_SHOULD_return_no_connected_output_WHEN_no_output_is_connected() {
  outputs="${A_DISCONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")
  numberOfConnectedOutputs=$(countNumberOfOutputs "${connectedOutputs}")

  assertEquals 0 "${numberOfConnectedOutputs}"
}

test_getConnectedOutputs_SHOULD_return_one_connected_output_WHEN_one_output_is_connected() {
  outputs="${A_CONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  numberOfConnectedOutputs=$(countNumberOfOutputs "${connectedOutputs}")
  assertEquals 1 "${numberOfConnectedOutputs}"
}

test_getConnectedOutputs_SHOULD_return_all_connected_outputs_WHEN_multiple_outputs_are_connected() {
  outputs=$(printf "%s\n%s\n%s" "${A_CONNECTED_OUTPUT}" "${ANOTHER_CONNECTED_OUTPUT}")

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  numberOfConnectedOutputs=$(countNumberOfOutputs "${connectedOutputs}")
  assertEquals 2 "${numberOfConnectedOutputs}"
}

test_getConnectedOutputs_SHOULD_return_output_name_as_first_field() {
  outputs="${A_CONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  firstField=$(echo "${connectedOutputs}" | cut --delimiter=, --fields=1)
  assertEquals "${A_CONNECTED_OUTPUT_NAME}" "${firstField}"
}

test_getConnectedOutputs_SHOULD_return_width_as_second_field() {
  outputs="${A_CONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  secondField=$(echo "${connectedOutputs}" | cut --delimiter=, --fields=2)
  assertEquals "${A_CONNECTED_OUTPUT_WIDTH}" "${secondField}"
}

test_getConnectedOuputs_SHOULD_return_height_as_third_field() {
  outputs="${A_CONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  thirdField=$(echo "${connectedOutputs}" | cut --delimiter=, --fields=3)
  assertEquals "${A_CONNECTED_OUTPUT_HEIGHT}" "${thirdField}"
}

test_getConnectedOutputs_SHOULD_return_top_as_fourth_field() {
  outputs="${A_CONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  fourthField=$(echo "${connectedOutputs}" | cut --delimiter=, --fields=4)
  assertEquals "${A_CONNECTED_OUTPUT_TOP}" "${fourthField}"
}

test_getConnectedOutputs_SHOULD_return_left_as_fifth_field() {
  outputs="${A_CONNECTED_OUTPUT}"

  connectedOutputs=$(getConnectedOutputs "${outputs}")

  fifthField=$(echo "${connectedOutputs}" | cut --delimiter=, --fields=5)
  assertEquals "${A_CONNECTED_OUTPUT_LEFT}" "${fifthField}"
}

countNumberOfOutputs() {
  echo -n "${1}" | grep --count '^'
}

oneTimeSetUp() {
  . src/share-screen
}

. shunit2
